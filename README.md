# AWS Route53 Terraform Importer

This project is a tool written in Go that allows you to import your existing AWS Route53 resources into a Terraform stack ready to be managed on your production environment.

The project creates a terraform stack using a public terraform module for AWS route53 resources. The module is available at [terraform-aws-route53](https://github.com/terraform-aws-modules/terraform-aws-route53).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Go 1.18 or higher
- AWS credentials configured on your local machine
- For testing purposes, you can use [Localstack](https://www.localstack.cloud/)

### Installation

1. Clone the repository
```bash
git clone git@gitlab.com:renemontilva/route53-stack-importer.git 
```
2. Navigate to the project directory
```bash
cd route53-stack-importer
```
3. Build the project
```bash
go build -o r53s
```

## Usage

To use the AWS Route53 Terraform Importer, you need to have your AWS credentials configured on your local machine.


The following commands are available:

* `assess` - Assesses the AWS Route53 resources and generates a report
* `import` - Imports the AWS Route53 resources into a Terraform stack

To assess the AWS Route53 resources and generate a report, run the built binary with the `assess` command

```bash
./r53s assess
```

To import the AWS Route53 resources into a Terraform stack, run the built binary with the `import` command

```bash
./r53s import 
```

For testing purposes with localstack, you can run the following command:

```bash
./r53s assess --endpoint="http://localhost:4566"
```

```bash
./r53s import --endpoint="http://localhost:4566"
```

This will start the process of importing your AWS Route53 resources into your Terraform stack.

The generated Terraform stack will be available in the user's home directory under the `GeneratedStacks` directory.

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/renemontilva/route53-stack-importer/-/blob/main/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/renemontilva/route53-stack-importer/-/blob/main/LICENSE) file for details
