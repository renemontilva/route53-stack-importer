/*
Copyright © 2024 Rene Montilva <renemontilva@gmail.com>
*/
package main

import (
	"github.com/renemontilva/route53-stack-importer/cmd"
)

func main() {
	cmd.Execute()
}
