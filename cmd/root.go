/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var awsRegion string
var awsEndpoint string

var rootCmd = &cobra.Command{
	Use:   "r53s",
	Short: "Terraform Stack Importer for Route53 hosted zones and records",
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVar(&awsRegion, "region", "us-east-1", "AWS region")
	rootCmd.PersistentFlags().StringVar(&awsEndpoint, "endpoint", "", "AWS endpoint")
}
