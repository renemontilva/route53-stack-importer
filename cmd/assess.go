/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"fmt"

	importer "github.com/renemontilva/route53-stack-importer/pkg"
	"github.com/spf13/cobra"
)

// assessCmd represents the assess command
var assessCmd = &cobra.Command{
	Use:   "assess",
	Short: "Assess Route53 hosted zones for importing into Terraform",
	Long: `Generates a Summary table of Route53 hosted zones and their records
		the following example shows how to use the command:
		$ terraform-stack-importer assess --profile my-profile --region us-west-2`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()
		awsCfg, err := importer.ConfigureAWS(awsEndpoint, awsRegion)
		if err != nil {
			fmt.Println("unable to load AWS configuration", err)
		}
		DNS := importer.NewDNS(awsCfg, importer.WithContext(ctx))
		assessmentSummary, err := DNS.ZonesAssessment()
		if err != nil {
			fmt.Println("unable to assess hosted zones", err)
		}
		assessmentSummary.ReportSummary()
	},
}

func init() {
	rootCmd.AddCommand(assessCmd)
}
