/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"fmt"

	importer "github.com/renemontilva/route53-stack-importer/pkg"
	"github.com/spf13/cobra"
)

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Import Route53 hosted zones and records and generate a Terraform stack folder",
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()
		awsCfg, err := importer.ConfigureAWS(awsEndpoint, awsRegion)
		if err != nil {
			fmt.Println("unable to load AWS configuration", err)
		}
		DNS := importer.NewDNS(awsCfg, importer.WithContext(ctx))
		err = DNS.ImportAllRecordZones()
		if err != nil {
			fmt.Println("unable to import hosted zones", err)
		}
		module, err := importer.NewRoute53Module(
			"terraform-aws-modules/route53/aws//modules/zones",
			"~> 2.0",
		)
		if err != nil {
			fmt.Println("unable to load module", err)
		}

		importZones := importer.NewImportZones(DNS, module)
		// Generate Imported Stack Structure
		err = importZones.GenerateImportedStack()
		if err != nil {
			fmt.Println("unable to generate imported stack", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(importCmd)
}
