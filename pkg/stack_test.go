package importer

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGenerateStack(t *testing.T) {
	testCases := []struct {
		desc string
	}{
		{
			desc: "Test Generate Structure",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			stack, err := NewStack("route53",
				"~> 1.0",
				"~> 5.0")
			if err != nil {
				t.Errorf("NewStack() error = %v", err)
				return
			}
			stack.GenerateStructure()
			expectedFolder := fmt.Sprintf("GeneratedStacks/route53_%s", time.Now().Format("20060102150405"))
			assert.Equal(t, stack.Name, expectedFolder)
			assert.Equal(t, stack.TerraformVersion, "~> 1.0")
			assert.Equal(t, stack.ProviderVersion, "~> 5.0")
			assert.Equal(t, fmt.Sprintf("%s/main.tf", expectedFolder), stack.ResourcesFilePath)
			assert.Equal(t, fmt.Sprintf("%s/variables.tf", expectedFolder), stack.VariableFilePath)
			assert.Equal(t, fmt.Sprintf("%s/terraform.auto.tfvars", expectedFolder), stack.TFvarsFilePath)
		})
	}
}
