package importer

import (
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
)

type ZoneAssessment struct {
	ZoneId  string
	Zone    string
	Records int64
}

type AssessmentSummary struct {
	Zones        []ZoneAssessment
	TotalZones   int64
	TotalRecords int64
}

// NewAssessmentSummary creates a new AssessmentSummary struct.
func NewAssessmentSummary() AssessmentSummary {
	return AssessmentSummary{}
}

// ZonesAssessment retrieves all hosted zones and records from AWS Route53 and returns an AssessmentSummary.
func (d *DNS) ZonesAssessment() (AssessmentSummary, error) {
	summary := NewAssessmentSummary()

	err := d.loadZones()
	if err != nil {
		return summary, err
	}
	for _, zone := range d.Zones {
		summary.Zones = append(summary.Zones, ZoneAssessment{
			ZoneId:  *zone.Id,
			Zone:    *zone.Name,
			Records: *zone.ResourceRecordSetCount,
		})
		summary.TotalZones++
		summary.TotalRecords += *zone.ResourceRecordSetCount
	}

	return summary, nil
}

// ReportSummary prints the AssessmentSummary to the console.
func (as *AssessmentSummary) ReportSummary() {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"ZoneId", "Zone", "Records"})
	rows := make([]table.Row, 0)
	for _, row := range as.Zones {
		rows = append(rows, table.Row{row.ZoneId, row.Zone, row.Records})
	}
	t.AppendRows(rows)
	t.AppendFooter(table.Row{"Total Zones", as.TotalZones, "Total Records", as.TotalRecords})
	t.Render()
}
