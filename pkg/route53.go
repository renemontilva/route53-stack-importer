// DNS struct interacts with AWS Route53 API and retrieve hosted zones and records.
package importer

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/route53"
	"github.com/aws/aws-sdk-go-v2/service/route53/types"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclwrite"
)

type Zone struct {
	types.HostedZone
	Records []types.ResourceRecordSet
	VPCs    *[]types.VPC
	Tags    []types.Tag
}

type DNS struct {
	Zones   []Zone
	client  *route53.Client
	context context.Context
}

type option func(*DNS) error

// NewDNS creates a new DNS struct with the provided AWS Config and options.
func NewDNS(cfg *aws.Config, options ...option) *DNS {
	d := &DNS{
		client: route53.NewFromConfig(*cfg),
	}
	for _, option := range options {
		_ = option(d)
	}
	return d
}

func WithContext(ctx context.Context) option {
	return func(d *DNS) error {
		d.context = ctx
		return nil
	}
}

// ImportAllRecordZones retrieves all hosted zones and records from AWS Route53.
func (d *DNS) ImportAllRecordZones() error {
	err := d.loadZones()
	if err != nil {
		return err
	}
	err = d.loadZoneRecords()
	if err != nil {
		return err
	}

	return nil
}

// GenerateImportFile generates the HCL file with the hosted zones and records.
func (d *DNS) GenerateImportFile() (*hclwrite.File, error) {
	f := hclwrite.NewEmptyFile()
	gohcl.EncodeIntoBody(d, f.Body())
	return nil, nil
}

// loadZones retrieves all hosted zones from AWS Route53.
func (d *DNS) loadZones() error {
	paginator := route53.NewListHostedZonesPaginator(d.client, &route53.ListHostedZonesInput{})
	for paginator.HasMorePages() {
		hostedZones, err := paginator.NextPage(d.context)
		if err != nil {
			return err
		}
		for _, hostedZone := range hostedZones.HostedZones {
			var vpcs []types.VPC
			sanitizedZoneID := sanitizeZoneID(*hostedZone.Id)
			resourceTag, err := d.client.ListTagsForResource(d.context, &route53.ListTagsForResourceInput{
				ResourceId:   &sanitizedZoneID,
				ResourceType: types.TagResourceTypeHostedzone,
			})
			if err != nil {
				return err
			}

			if hostedZone.Config.PrivateZone {
				getHostedZone, err := d.client.GetHostedZone(d.context,
					&route53.GetHostedZoneInput{
						Id: &sanitizedZoneID,
					})
				if err != nil {
					return err
				}
				vpcs = getHostedZone.VPCs
			}

			d.Zones = append(d.Zones, Zone{
				HostedZone: hostedZone,
				Tags:       resourceTag.ResourceTagSet.Tags,
				VPCs:       &vpcs,
			},
			)
		}
	}
	return nil
}

// loadZoneRecords retrieves all records from the hosted zones.
func (d *DNS) loadZoneRecords() error {

	for id := range d.Zones {
		paginator := route53.NewListResourceRecordSetsPaginator(d.client, &route53.ListResourceRecordSetsInput{
			HostedZoneId: d.Zones[id].Id,
		})
		for paginator.HasMorePages() {
			records, err := paginator.NextPage(d.context)
			if err != nil {
				return err
			}

			for i := range records.ResourceRecordSets {
				if records.ResourceRecordSets[i].Type == types.RRTypeSoa || records.ResourceRecordSets[i].Type == types.RRTypeNs {
					continue
				} else {
					d.Zones[id].Records = append(d.Zones[id].Records, records.ResourceRecordSets[i])
				}
			}

		}
	}
	return nil
}
