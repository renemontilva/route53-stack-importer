package importer

import (
	"embed"
	"fmt"
	"os"
	"strings"
	"text/template"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/zclconf/go-cty/cty"
)

type Import struct {
	To string `hcl:"to"`
	Id string `hcl:"id"`
}

type ImportZones struct {
	Imports []Import `hcl:"import,block"`
	Module  *Route53Module
	DNS     *DNS
}

//go:embed templates/*.tmpl
var templateFS embed.FS

func NewImportZones(dns *DNS, module *Route53Module) *ImportZones {
	var importZones ImportZones
	importZones.Module = module
	importZones.DNS = dns

	return &importZones
}

// GenerateBlockToHCL generates the import block to the HCL file.
func (i *ImportZones) GenerateBlockToHCL(stackFolder string) error {

	for _, zone := range i.DNS.Zones {
		file, err := os.Create(fmt.Sprintf("%s/%s", stackFolder, fmt.Sprintf("%s_%s", sanitizeZoneName(*zone.Name), TF_IMPORTS_FILE)))
		if err != nil {
			return err
		}
		defer file.Close()

		f := hclwrite.NewEmptyFile()
		rootBody := f.Body()

		zoneName := strings.TrimSuffix(*zone.Name, ".")
		importBlock := rootBody.AppendNewBlock("import", []string{})
		importBody := importBlock.Body()
		importBody.SetAttributeTraversal("to", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "module",
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf("%s_zone", sanitizeZoneName(*zone.Name)),
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf(`aws_route53_zone.this["%v"]`, zoneName),
			},
		})
		importBody.SetAttributeValue("id", cty.StringVal(sanitizeZoneID(*zone.Id)))
		rootBody.AppendNewline()

		for _, record := range zone.Records {
			var id string
			if record.Type != "NS" && record.Type != "SOA" {

				recordName := strings.TrimSuffix(*record.Name, ".")
				prefixRecordName := strings.TrimSuffix(strings.TrimSuffix(*record.Name, *zone.Name), ".")
				recordIndex := fmt.Sprintf(`aws_route53_record.this["%s %s"]`, prefixRecordName, record.Type)
				if record.SetIdentifier != nil {
					recordIndex = fmt.Sprintf(`aws_route53_record.this["%s %s %s"]`, prefixRecordName, record.Type, *record.SetIdentifier)
				}

				importBlock := rootBody.AppendNewBlock("import", []string{})
				importBody := importBlock.Body()
				if record.SetIdentifier != nil {
					id = fmt.Sprintf("%s_%s_%s_%s", sanitizeZoneID(*zone.Id), recordName, record.Type, *record.SetIdentifier)
				}
				if record.Name == nil && record.SetIdentifier != nil {
					id = fmt.Sprintf("%s__%s_%s", sanitizeZoneID(*zone.Id), record.Type, *record.SetIdentifier)
				}
				if record.SetIdentifier == nil {
					id = fmt.Sprintf("%s_%s_%s", sanitizeZoneID(*zone.Id), recordName, record.Type)
				}
				if record.Name == nil && record.SetIdentifier == nil {
					id = fmt.Sprintf("%s__%s", sanitizeZoneID(*zone.Id), record.Type)
				}

				importBody.SetAttributeTraversal("to", hcl.Traversal{
					hcl.TraverseRoot{
						Name: "module",
					},
					hcl.TraverseAttr{
						Name: fmt.Sprintf(`%s_records["%s"]`, sanitizeZoneName(*zone.Name), strings.TrimSuffix(*zone.Name, ".")),
					},
					hcl.TraverseAttr{
						Name: recordIndex,
					},
				})
				importBody.SetAttributeValue("id", cty.StringVal(id))
				rootBody.AppendNewline()
			}
		}

		_, err = f.WriteTo(file)
		if err != nil {
			return err
		}
	}

	return nil
}

// GenerateVersionsToHCL generates the terraform block to the HCL file.
func (i *ImportZones) GenerateVersionsToHCL(versionsFile string) error {
	file, err := os.Create(versionsFile)
	if err != nil {
		return err
	}
	defer file.Close()

	f := hclwrite.NewEmptyFile()
	rootBody := f.Body()
	terraformBlock := rootBody.AppendNewBlock("terraform", []string{})
	terraformBody := terraformBlock.Body()
	terraformBody.SetAttributeValue("required_version", cty.StringVal(">= 1.6"))
	requiredProviderBlock := terraformBody.AppendNewBlock("required_providers", []string{})
	requiredProviderBody := requiredProviderBlock.Body()
	requiredProviderBody.SetAttributeValue("aws", cty.ObjectVal(map[string]cty.Value{
		"source":  cty.StringVal("hashicorp/aws"),
		"version": cty.StringVal("~> 5.0"),
	}))
	_, err = file.Write(f.Bytes())
	if err != nil {
		return err
	}
	return nil
}

// GenerateProvidersToHCL generates the provider block to the HCL file.
func (i *ImportZones) GenerateProvidersToHCL(providersFile string) error {
	file, err := os.Create(providersFile)
	if err != nil {
		return err
	}
	defer file.Close()

	f := hclwrite.NewEmptyFile()
	rootBody := f.Body()
	rootBody.AppendNewBlock("provider", []string{"aws"})
	_, err = file.Write(f.Bytes())
	if err != nil {
		return err
	}
	return nil
}

// GenerateResourcesToHCL generates the resources block to the HCL file.
func (i *ImportZones) GenerateResourcesToHCL(stackFolder string) error {

	for _, zone := range i.DNS.Zones {
		zoneName := sanitizeZoneName(*zone.Name)
		file, err := os.Create(fmt.Sprintf("%s/%s", stackFolder, fmt.Sprintf("%s_%s", sanitizeZoneName(*zone.Name), TF_RESOURCES_FILE)))
		if err != nil {
			return err
		}
		defer file.Close()
		f := hclwrite.NewEmptyFile()
		rootBody := f.Body()
		moduleZonesBlock := rootBody.AppendNewBlock("module", []string{fmt.Sprintf("%s_zone", zoneName)})
		moduleZonesBody := moduleZonesBlock.Body()
		moduleZonesBody.SetAttributeValue("source", cty.StringVal("terraform-aws-modules/route53/aws//modules/zones"))
		moduleZonesBody.SetAttributeValue("version", cty.StringVal(i.Module.Version))
		moduleZonesBody.SetAttributeTraversal("zones", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "var",
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf("%s_zone", zoneName),
			},
		})
		rootBody.AppendNewline()
		moduleRecordsBlock := rootBody.AppendNewBlock("module", []string{fmt.Sprintf("%s_records", zoneName)})
		moduleRecordsBody := moduleRecordsBlock.Body()
		moduleRecordsBody.SetAttributeTraversal("for_each", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "var",
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf("%s_zone", zoneName),
			},
		})
		moduleRecordsBody.SetAttributeValue("source", cty.StringVal("terraform-aws-modules/route53/aws//modules/records"))
		moduleRecordsBody.SetAttributeValue("version", cty.StringVal(i.Module.Version))
		moduleRecordsBody.SetAttributeTraversal("zone_id", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "each",
			},
			hcl.TraverseAttr{
				Name: "value",
			},
			hcl.TraverseAttr{
				Name: "id",
			},
		})
		moduleRecordsBody.SetAttributeTraversal("records", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "each",
			},
			hcl.TraverseAttr{
				Name: "value",
			},
			hcl.TraverseAttr{
				Name: "records",
			},
		})
		moduleRecordsBody.SetAttributeTraversal("depends_on", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "[module",
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf("%s_zone]", zoneName),
			},
		})
		rootBody.AppendNewline()

		_, err = f.WriteTo(file)
		if err != nil {
			return err
		}
	}

	return nil
}

// GenerateVariablesToHCL generates the variables block to the HCL file.
func (i *ImportZones) GenerateVariablesToHCL(stackFolder string) error {

	for _, zone := range i.DNS.Zones {
		zoneName := sanitizeZoneName(*zone.Name)
		file, err := os.Create(fmt.Sprintf("%s/%s", stackFolder, fmt.Sprintf("%s_%s", sanitizeZoneName(*zone.Name), TF_VARIABLES_FILE)))
		if err != nil {
			return nil
		}
		defer file.Close()

		f := hclwrite.NewEmptyFile()
		rootBody := f.Body()
		zonesBlock := rootBody.AppendNewBlock("variable", []string{fmt.Sprintf("%s_zone", zoneName)})
		zonesBlock.Body().SetAttributeTraversal("type", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "any",
			},
		})
		zonesBlock.Body().SetAttributeValue("description", cty.StringVal(fmt.Sprintf("Object of zone %s", *zone.Name)))
		rootBody.AppendNewline()

		_, err = file.Write(f.Bytes())
		if err != nil {
			return err
		}
	}

	return nil
}

// GenerateOutputsToHCL generates the outputs block to the HCL file.
func (i *ImportZones) GenerateOutputsToHCL(stackFolder string) error {

	for _, zone := range i.DNS.Zones {
		zoneName := sanitizeZoneName(*zone.Name)
		file, err := os.Create(fmt.Sprintf("%s/%s", stackFolder, fmt.Sprintf("%s_%s", sanitizeZoneName(*zone.Name), TF_OUTPUTS_FILE)))
		if err != nil {
			return err
		}
		defer file.Close()

		f := hclwrite.NewEmptyFile()
		rootBody := f.Body()

		zonesBlock := rootBody.AppendNewBlock("output", []string{zoneName})
		zonesBlock.Body().SetAttributeTraversal("value", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "module",
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf("%s_zone", zoneName),
			},
		})
		zonesBlock.Body().SetAttributeValue("description", cty.StringVal("List of zones' parameters"))
		rootBody.AppendNewline()
		recordsBlock := rootBody.AppendNewBlock("output", []string{fmt.Sprintf("%s_records", zoneName)})
		recordsBlock.Body().SetAttributeTraversal("value", hcl.Traversal{
			hcl.TraverseRoot{
				Name: "module",
			},
			hcl.TraverseAttr{
				Name: fmt.Sprintf("%s_records", zoneName),
			},
		})
		recordsBlock.Body().SetAttributeValue("description", cty.StringVal("List of records' parameters"))
		rootBody.AppendNewline()

		_, err = file.Write(f.Bytes())
		if err != nil {
			return err
		}
	}

	return nil
}

// GenerateTFvarsToHCL generates the terraform.tfvars file to the HCL file.
func (i *ImportZones) GenerateTFvarsToHCL(stackPath string, tfvarsFileSuffix string) error {
	for _, zone := range i.DNS.Zones {
		zoneName := sanitizeZoneName(*zone.Name)
		file, err := os.Create(fmt.Sprintf("%s/%s", stackPath, fmt.Sprintf("%s.%s", zoneName, tfvarsFileSuffix)))
		if err != nil {
			return err
		}
		t, err := template.New("terraform.tfvars.tmpl").Funcs(template.FuncMap{
			"sanitizeZoneName": func(name string) string {
				return strings.TrimSuffix(name, ".")
			},
			"sanitizeZoneID": func(id string) string {
				return sanitizeZoneID(id)
			},
			"sanitizeVariableName": func(name string) string {
				return sanitizeZoneName(name)
			},
			"sanitizeRecordName": func(name string) string {
				return strings.TrimSuffix(strings.TrimSuffix(name, *zone.Name), ".")
			},
			"dereferenceTags": func(key *string) string {
				return *key
			},
		}).ParseFS(templateFS, "templates/terraform.tfvars.tmpl")
		if err != nil {
			return err
		}
		err = t.Execute(file, zone)
		if err != nil {
			return err
		}

	}
	return nil
}

// GenerateImportedStack generates the imported stack structure.
func (i *ImportZones) GenerateImportedStack() error {
	stack, err := NewStack("route53",
		"~> 1.0",
		"~> 5.0")
	if err != nil {
		return fmt.Errorf("failed on NewStack(): %w", err)
	}
	err = stack.GenerateStructure()
	if err != nil {
		return fmt.Errorf("failed on GenerateStructure(): %w", err)
	}
	stackFolder := fmt.Sprintf("%s/%s", stack.LocalPath, stack.Name)

	err = i.GenerateBlockToHCL(stackFolder)
	if err != nil {
		return err
	}
	err = i.GenerateVersionsToHCL(fmt.Sprintf("%s/%s", stackFolder, TF_VERSIONS_FILE))
	if err != nil {
		return err
	}
	err = i.GenerateProvidersToHCL(fmt.Sprintf("%s/%s", stackFolder, TF_PROVIDER_FILE))
	if err != nil {
		return err
	}
	err = i.GenerateVariablesToHCL(stackFolder)
	if err != nil {
		return err
	}
	err = i.GenerateResourcesToHCL(stackFolder)
	if err != nil {
		return err
	}
	err = i.GenerateTFvarsToHCL(stackFolder, "auto.tfvars")
	if err != nil {
		return err
	}
	err = i.GenerateOutputsToHCL(stackFolder)
	if err != nil {
		return err
	}

	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"Imported Stack"})
	rows := make([]table.Row, 0)
	rows = append(rows, table.Row{stack.Name})
	t.AppendRows(rows)
	t.Render()

	return nil
}

// sanitizeZoneName removes the trailing dot and replaces the first dot with an underscore
func sanitizeZoneName(name string) string {
	return strings.Replace(strings.TrimSuffix(name, "."), ".", "_", -1)
}

// sanitizeZoneID remove the prefix /hostedzone/ from the zone ID
func sanitizeZoneID(id string) string {
	return strings.TrimPrefix(id, "/hostedzone/")
}
