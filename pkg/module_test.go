package importer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRoute53Module(t *testing.T) {
	testCases := []struct {
		filePath string
		desc     string
	}{
		{
			desc: "should return a Route53Module",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			module, err := NewRoute53Module("terraform-aws-modules/route53/aws//modules/zones", "~> 2.0")
			if err != nil {
				t.Errorf("NewRoute53Module() error = %v", err)
				return
			}
			assert.Equal(t, "terraform-aws-modules/route53/aws//modules/zones", module.Source)
			assert.Equal(t, "~> 2.0", module.Version)
		})
	}
}
