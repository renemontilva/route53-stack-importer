// Stack generates the structure of the Terraform stack and the generated files are the
// versions.tf, provider.tf, main.tf, variables.tf, and terraform.auto.tfvars.
package importer

import (
	"fmt"
	"os"
	"time"
)

const (
	INITIAL_FOLDER    = "GeneratedStacks"
	TF_IMPORTS_FILE   = "imports.tf"
	TF_PROVIDER_FILE  = "providers.tf"
	TF_VERSIONS_FILE  = "versions.tf"
	TF_RESOURCES_FILE = "main.tf"
	TF_VARIABLES_FILE = "variables.tf"
	TF_OUTPUTS_FILE   = "outputs.tf"
	TF_VARS_FILE      = "terraform.auto.tfvars"
)

type Stack struct {
	Name              string
	LocalPath         string
	TerraformVersion  string
	ProviderVersion   string
	ResourcesFilePath string
	VariableFilePath  string
	TFvarsFilePath    string
}

type stackOption func(*Stack) error

func WithLocalPath(path string) stackOption {
	return func(s *Stack) error {
		s.LocalPath = path
		return nil
	}
}

// NewStack creates a new Stack struct with the provided name, Terraform version, and provider version.
func NewStack(name string, terraformVersion string, providerVersion string, option ...stackOption) (*Stack, error) {
	var stack Stack

	stackName := fmt.Sprintf("%s/%s_%s", INITIAL_FOLDER, name, time.Now().Format("20060102150405"))
	path, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}

	stack = Stack{
		Name:              stackName,
		LocalPath:         path,
		TerraformVersion:  terraformVersion,
		ProviderVersion:   providerVersion,
		ResourcesFilePath: fmt.Sprintf("%s/%s", stackName, TF_RESOURCES_FILE),
		VariableFilePath:  fmt.Sprintf("%s/%s", stackName, TF_VARIABLES_FILE),
		TFvarsFilePath:    fmt.Sprintf("%s/%s", stackName, TF_VARS_FILE),
	}

	for _, opt := range option {
		err := opt(&stack)
		if err != nil {
			return nil, err
		}
	}

	return &stack, nil
}

// GenerateStructure generates the structure of the Terraform stack.
func (s *Stack) GenerateStructure() error {
	err := createInitialFolder(s.LocalPath)
	if err != nil {
		return nil
	}

	err = os.MkdirAll(fmt.Sprintf("%s/%s", s.LocalPath, s.Name), 0755)
	if err != nil {
		return err
	}

	return nil
}

// createInitialFolder creates the initial folder for the Terraform stack.
func createInitialFolder(path string) error {

	err := os.MkdirAll(fmt.Sprintf("%s/%s", path, INITIAL_FOLDER), 0755)
	if err != nil {
		return err
	}
	return nil
}
