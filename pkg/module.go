package importer

type Route53Module struct {
	Source  string
	Version string
}

func NewRoute53Module(source, version string) (*Route53Module, error) {

	return &Route53Module{
		Source:  source,
		Version: version,
	}, nil
}
