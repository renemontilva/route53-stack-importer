package importer

import (
	"context"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
)

func TestNewImportZones(t *testing.T) {
	testCases := []struct {
		desc string
	}{
		{
			desc: "Test Import Zones features",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			awsEndpoint := "http://localhost:4566"
			awsRegion := "us-east-1"

			customResolver := aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
				if awsEndpoint != "" {
					return aws.Endpoint{
						PartitionID:   "aws",
						URL:           awsEndpoint,
						SigningRegion: awsRegion,
					}, nil
				}
				return aws.Endpoint{}, &aws.EndpointNotFoundError{}
			})

			awsCfg, err := config.LoadDefaultConfig(context.TODO(),
				config.WithRegion(awsRegion),
				config.WithEndpointResolverWithOptions(customResolver),
			)
			if err != nil {
				t.Fatal(err)
			}

			route53 := NewDNS(&awsCfg, WithContext(context.Background()))
			route53.ImportAllRecordZones()
			module, err := NewRoute53Module(
				"terraform-aws-modules/route53/aws//modules/zones",
				"~> 2.0")
			if err != nil {
				t.Fatal(err)
			}

			importZones := NewImportZones(route53, module)
			// Generate Imported Stack Structure
			err = importZones.GenerateImportedStack()
			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
