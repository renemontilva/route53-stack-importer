package importer

import (
	"context"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/stretchr/testify/assert"
)

func TestImportAllRecordZones(t *testing.T) {
	testCases := []struct {
		desc               string
		awsEndpoint        string
		awsRegion          string
		awsSecretAccessKey string
		awsAccessKeyID     string
	}{
		{
			desc:               "Test Zones Assessment",
			awsEndpoint:        "http://localhost:4566",
			awsRegion:          "us-east-1",
			awsSecretAccessKey: "test",
			awsAccessKeyID:     "test",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			customResolver := aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
				if tC.awsEndpoint != "" {
					return aws.Endpoint{
						PartitionID:   "aws",
						URL:           tC.awsEndpoint,
						SigningRegion: tC.awsRegion,
					}, nil
				}
				return aws.Endpoint{}, &aws.EndpointNotFoundError{}
			})

			awsCfg, err := config.LoadDefaultConfig(context.TODO(),
				config.WithRegion(tC.awsRegion),
				config.WithEndpointResolverWithOptions(customResolver),
			)
			if err != nil {
				t.Fatal(err)
			}
			route53 := NewDNS(&awsCfg, WithContext(context.Background()))
			err = route53.ImportAllRecordZones()
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, 4, len(route53.Zones))
		})
	}
}
