package importer

import (
	"context"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/stretchr/testify/assert"
)

func TestZonesAssessment(t *testing.T) {
	testCases := []struct {
		desc               string
		awsEndpoint        string
		awsRegion          string
		awsSecretAccessKey string
		awsAccessKeyID     string
	}{
		{
			desc:               "Test Zones Assessment",
			awsEndpoint:        "http://localhost:4566",
			awsRegion:          "us-east-1",
			awsSecretAccessKey: "test",
			awsAccessKeyID:     "test",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			customResolver := aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
				if tC.awsEndpoint != "" {
					return aws.Endpoint{
						PartitionID:   "aws",
						URL:           tC.awsEndpoint,
						SigningRegion: tC.awsRegion,
					}, nil
				}
				return aws.Endpoint{}, &aws.EndpointNotFoundError{}
			})

			awsCfg, err := config.LoadDefaultConfig(context.TODO(),
				config.WithRegion(tC.awsRegion),
				config.WithEndpointResolverWithOptions(customResolver),
			)
			if err != nil {
				t.Fatal(err)
			}
			route53 := NewDNS(&awsCfg, WithContext(context.Background()))
			assessmentSummary, err := route53.ZonesAssessment()
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, int64(4), assessmentSummary.TotalZones)
			assert.Equal(t, int64(19), assessmentSummary.TotalRecords)
			assert.Equal(t, "test.com.", assessmentSummary.Zones[0].Zone)
		})
	}
}
