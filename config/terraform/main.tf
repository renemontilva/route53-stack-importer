locals {
  zones = merge(var.test_zone)
}

module "zones" {
  source  = "terraform-aws-modules/route53/aws//modules/zones"
  version = "~> 2.0"
  zones   = local.zones
  tags = {
    Terraform = "true"
  }
}

module "records" {
  for_each     = local.zones
  source       = "terraform-aws-modules/route53/aws//modules/records"
  version      = "~> 2.0"
  zone_name    = module.zones.route53_zone_name[each.key]
  private_zone = each.value.private_zone
  records      = each.value.records
  depends_on   = [module.zones]
}
