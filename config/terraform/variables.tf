variable "test_zone" {
  type        = any
  description = "A map of test.com zone configurations"
  default     = {}
}

variable "lastminute_zone" {
  type        = any
  description = "A map of lastminute.com zone configurations"
  default     = {}
}
