test_zone = {
  "test.com" = {
    comment      = "Test zone"
    private_zone = false
    records = [
      {
        name = "apigateway1"
        type = "A"
        alias = {
          name    = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id = "ZLY8HYME6SFAD"
        }
      },
      {
        name = "nameserver1"
        type = "A"
        ttl  = 3600
        records = [
          "10.10.10.10",
        ]
      },
    ]
  }
  "test.com.ve" = {
    comment      = "Test zone"
    private_zone = false
    records = [
      {
        name = "apigateway1"
        type = "A"
        alias = {
          name    = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id = "ZLY8HYME6SFAD"
        }
      },
      {
        name = "nameserver1"
        type = "A"
        ttl  = 3600
        records = [
          "10.10.10.10",
        ]
      },
    ]
  }
  "private.es" = {
    domain_name  = "private.es"
    private_zone = true
    comment      = "Test zone"
    vpc = [
      {
        vpc_id = "vpc-12345678"
      }
    ]
    records = [
      {
        name = "apigateway1"
        type = "A"
        alias = {
          name    = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id = "ZLY8HYME6SFAD"
        }
      },
      {
        name    = "nameserver1"
        type    = "A"
        ttl     = 3600
        records = ["10.0.0.1"]
      }
    ]
  }
  "test.it" = {
    comment      = "Test zone"
    private_zone = false
    records = [
      {
        name = "apigateway1"
        type = "A"
        alias = {
          name    = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id = "ZLY8HYME6SFAD"
        }
      },
      {
        name    = "nameserver1"
        type    = "A"
        ttl     = 3600
        records = ["10.0.0.1"]
      }
    ]
  }
  "test.fr" = {
    comment      = "Test zone"
    private_zone = false
    records = [
      {
        name           = "geo"
        type           = "CNAME"
        set_identifier = "europe"
        geolocation_routing_policy = {
          continent = "EU"
        }
        records = ["geo.test.fr"]
      },
      {
        name           = "test_weighted"
        type           = "CNAME"
        records        = ["weighted.test.fr"]
        set_identifier = "weighted"
        weighted_routing_policy = {
          weight = 100
        }
      },
      {
        name            = "failover-primary"
        type            = "A"
        set_identifier  = "failover-primary"
        health_check_id = "123456"
        alias = {
          name    = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id = "ZLY8HYME6SFAD"
        }
        failover_routing_policy = {
          type = "PRIMARY"
        }
      },
      {
        name           = "failover-secondary"
        type           = "A"
        set_identifier = "failover-secondary"
        alias = {
          name    = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id = "ZLY8HYME6SFAD"
        }
        failover_routing_policy = {
          type = "SECONDARY"
        }
      },
      {
        name           = "latency-test"
        type           = "A"
        set_identifier = "latency-test"
        alias = {
          name                   = "d-10qxlbvagl.execute-api.eu-west-1.amazonaws.com"
          zone_id                = "ZLY8HYME6SFAD"
          evaluate_target_health = true
        }
        latency_routing_policy = {
          region = "eu-west-1"
        }
      }
    ]
  }
}

